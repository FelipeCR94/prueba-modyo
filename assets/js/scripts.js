//consumo API
const peticion = async () => {
  //número máximo de items
  let maxTest = 4;
  //consulta al API de usuarios
  const respUsuarios = await fetch('https://jsonplaceholder.typicode.com/users');
  //consulta al API de testimonios
  const respTestimonio = await fetch('https://jsonplaceholder.typicode.com/posts');
  //obtiene el json para cada API
  const jsonU = await respUsuarios.json();
  const jsonT = await respTestimonio.json();
  //obtiene el id del slider
  const slider = document.getElementById('carousel-api');
  //genera un div item con el contenido de cada testimonio dentro del slider
  for(i = 0; i < maxTest; i++){
    slider.innerHTML += `<div class='item'>
        <img src='./assets/images/person_${jsonU[i].id}.jpg'>
        <div class='text'>
          <p>${jsonT[i].body}</p>
          <span>${jsonU[i].name}</span>
        </div>
      </div>`;
  }
}
//llamado para cargar los testimonios
peticion();
setTimeout(function(){
  //ejecuta el carousel de la librería de owl-carousel
  $('.owl-carousel').owlCarousel({
      loop:true,
      margin:10,
      nav:false,
      dots:true,
      autoplay:true,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:1
          },
          1000:{
              items:1
          }
      }
  })
},1000)

//animaciones

ScrollReveal().reveal('.section-home', { delay: 500 });
ScrollReveal().reveal('.section-home .jumbotron', {
  delay: 600,
  distance: '50px',
  origin: 'bottom'
});
ScrollReveal().reveal('.section-features .row-item .card:nth-child(1)', {
  delay: 600,
  distance: '50px',
  origin: 'left'
});
ScrollReveal().reveal('.section-features .row-item .card:nth-child(2)', {
  delay: 650,
  distance: '50px',
  origin: 'left'
});
ScrollReveal().reveal('.section-features .row-item .card:nth-child(3)', {
  delay: 700,
  distance: '50px',
  origin: 'left'
});
ScrollReveal().reveal('.section-features .row-item .card:nth-child(4)', {
  delay: 750,
  distance: '50px',
  origin: 'left'
});
ScrollReveal().reveal('.section-features .row-item .card:nth-child(5)', {
  delay: 800,
  distance: '50px',
  origin: 'left'
});
ScrollReveal().reveal('.section-features .row-item .card:nth-child(6)', {
  delay: 850,
  distance: '50px',
  origin: 'left'
});
ScrollReveal().reveal('.section-prototypes .image-container', {
  delay: 600,
  distance: '50px',
  origin: 'left'
});
ScrollReveal().reveal('.section-prototypes .jumbotron', {
  delay: 700,
  distance: '50px',
  origin: 'right'
});
ScrollReveal().reveal('.section-prototypes .jumbotron .card', {
  delay: 1000,
  distance: '50px',
  origin: 'bottom'
});
ScrollReveal().reveal('.section-about-us img', {
  delay: 800,
  distance: '50px',
  origin: 'left'
});
ScrollReveal().reveal('.section-about-us .jumbotron', {
  delay: 900,
  distance: '50px',
  origin: 'right'
});
ScrollReveal().reveal('.section-testimonial', {
  delay: 1000,
  distance: '50px',
  origin: 'top'
});
ScrollReveal().reveal('.section-contact .container-form', {
  delay: 1000,
  distance: '50px',
  origin: 'left'
});
