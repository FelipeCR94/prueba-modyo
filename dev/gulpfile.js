'use strict';
const { series, parallel, watch, gulp, src, dest, task } = require('gulp');
const sass = require('gulp-sass'),
    cleanCss = require('gulp-clean-css');

sass.compiler = require('node-sass');

//tarea para compilar estilos .scss a .css
function estilos(){
  return(
    src('./sass/main.scss')
    .pipe(sass())
    .pipe(sass().on('error', sass.logError))
    .pipe(dest('./../assets/css/'))
  )
}
//tarea que escucha constantemente los cambios de los archivos .scss

function watchEstilos(){
  return(
    watch('./sass/*.scss', series('estilos'))
  )
}


//minifica css
function compileScss(){
  return(
    src('./../assets/css/*.css')
    .pipe(cleanCss())
    .pipe(cleanCss().on('error', cleanCss.logError))
    .pipe(dest('./../assets/css/'))
  )
}

//tarea que escucha constantemente los cambios del main.css y lo minifica
function watchCss(){
  return(
    watch('./../assets/css/*.css', series('compileScss'))
  )
}




exports.estilos = estilos;
exports.compileScss = compileScss;
exports.default = series(estilos, watchEstilos);
